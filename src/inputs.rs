//! Set of usable inputs inspired by [Seagraphy](https://github.com/SeaQL/seaography).

use async_graphql::InputObject;
use eyre::{OptionExt, Result};
use uuid::Uuid;

#[derive(Clone, Debug, PartialEq, PartialOrd, Ord, Eq, InputObject)]
pub struct UserFilterInput {
    pub id: Option<Uuid>,
}

pub fn try_user_to_filter_by(filter_opt: Option<UserFilterInput>, user_id_opt: Option<Uuid>) -> Result<Uuid> {
    filter_opt
        .and_then(|filter| filter.id)
        .or(user_id_opt)
        .ok_or_eyre("Failed to get user to filter by from filter or authorization.")
}

pub mod pagination {
    #![allow(clippy::module_name_repetitions)]

    use async_graphql::{InputObject, OneofObject};

    pub type Cursor = String;

    /// Different variants of [`PaginationInput`]:
    /// - [`CursorInput`],
    /// - [`PageInput`],
    /// - [`OffsetInput`].
    #[derive(Clone, Debug, OneofObject)]
    pub enum PaginationInput {
        Cursor(CursorInput),
        Page(PageInput),
        Offset(OffsetInput),
    }

    /// Cursor related inputs.
    #[derive(Clone, Debug, InputObject)]
    pub struct CursorInput {
        /// Optional cursor name.
        pub cursor: Option<Cursor>,
        /// Limit number of items per page.
        pub limit: u64,
    }

    /// Offest related inputs.
    #[derive(Clone, Debug, InputObject)]
    pub struct OffsetInput {
        /// Which offset to use.
        pub offset: u64,
        /// Limit number of items per page.
        pub limit: u64,
    }

    /// Page related inputs.
    #[derive(Clone, Debug, InputObject)]
    pub struct PageInput {
        /// Which page to load.
        pub page: u64,
        /// Limit number of items per page.
        pub limit: u64,
    }

    impl PaginationInput {
        #[must_use]
        pub fn cursor(&self) -> Option<Cursor> {
            match self {
                Self::Cursor(CursorInput { cursor, .. }) => cursor.clone(),
                Self::Page(_) | Self::Offset(_) => None,
            }
        }

        #[must_use]
        pub fn page(&self) -> Option<u64> {
            match self {
                Self::Cursor(_) => None,
                Self::Page(PageInput { page, .. }) => Some(*page),
                Self::Offset(OffsetInput { offset, limit }) => Some(offset / limit),
            }
        }

        #[must_use]
        pub fn offset(&self) -> Option<u64> {
            match self {
                Self::Cursor(_) => None,
                Self::Page(PageInput { page, limit }) => Some(page * limit),
                Self::Offset(OffsetInput { offset, .. }) => Some(*offset),
            }
        }

        #[must_use]
        pub const fn limit(&self) -> u64 {
            match self {
                Self::Offset(OffsetInput { limit, .. }) |
                Self::Page(PageInput { limit, .. }) |
                Self::Cursor(CursorInput { limit, .. }) => *limit,
            }
        }
    }

    impl Default for PaginationInput {
        fn default() -> Self {
            Self::Offset(OffsetInput::default())
        }
    }

    impl Default for OffsetInput {
        fn default() -> Self {
            Self { offset: 0, limit: 100 }
        }
    }
}

pub mod sorting {
    use async_graphql::{Enum, InputObject};
    use derive_more::Display;

    pub type Field = String;

    /// Order related inputs.
    #[derive(Clone, Debug, InputObject)]
    pub struct OrderInput {
        pub field: Field,
        pub direction: Direction,
    }

    /// Sorting direction.
    #[derive(Clone, Debug, Copy, PartialEq, PartialOrd, Ord, Eq, Enum, Display)]
    pub enum Direction {
        #[display("asc")]
        Asc,
        #[display("desc")]
        Desc,
    }
}
