use async_graphql::Context;
use eyre::{eyre, Result};
use jwt_authorizer::{JwtClaims, RegisteredClaims};
use uuid::Uuid;

/// Returns `user_id` safely extracted from a [`Context`].
///
/// # Errors
///
/// This function will return an error if `user_id` absent in context or
/// if it's in inappropriate format.
pub fn extract_user_id(context: &Context<'_>) -> Result<Uuid> {
    Ok(Uuid::parse_str(
        context
            .data_opt::<JwtClaims<RegisteredClaims>>()
            .ok_or_else(|| eyre!("unauthorized"))?
            .0
            .sub
            .as_ref()
            .ok_or_else(|| eyre!("sub missing"))?
            .as_str(),
    )?)
}

/// Returns `Some(user_id)` safely extracted from a [`Context`] if presented
/// and `None` if it's not.
///
/// # Errors
///
/// This function will return an error if `user_id` is in wrong format.
pub fn extract_user_id_opt(context: &Context<'_>) -> Result<Option<Uuid>> {
    if let Some(claims) = context.data_opt::<JwtClaims<RegisteredClaims>>() {
        Ok(Some(Uuid::parse_str(
            claims.0.sub.as_ref().ok_or_else(|| eyre!("sub missing"))?.as_str(),
        )?))
    } else {
        Ok(None)
    }
}
