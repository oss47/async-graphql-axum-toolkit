use async_graphql::{OutputType, SimpleObject};

pub type Cursor = String;

/// Composition of pagination inputs [`CursorInput`], [`PageInput`], [`OffsetInput`].
#[derive(Clone, Debug, Default, SimpleObject)]
#[graphql(shareable)]
pub struct PaginationOutput<V: OutputType> {
    pub value: V,
    pub cursor: Option<Cursor>,
    pub page: Option<u64>,
    pub offset: Option<u64>,
}
