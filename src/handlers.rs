use async_graphql::{http::ALL_WEBSOCKET_PROTOCOLS, Data, ObjectType, Schema, SubscriptionType};
use async_graphql_axum::{GraphQLProtocol, GraphQLRequest, GraphQLResponse, GraphQLWebSocket};
use axum::{
    extract::{State, WebSocketUpgrade},
    response::IntoResponse,
    Extension,
};
use jsonwebtoken::{DecodingKey, Validation};
use jwt_authorizer::{JwtClaims, RegisteredClaims};
use serde::Deserialize;
use tracing_attributes::instrument;

pub const GRAPHQL_ROOT_PATH: &str = "/gql";
pub const GRAPHQL_WS_ROOT_PATH: &str = "/ws";

#[instrument(skip(schema, req), level = "trace")]
pub async fn graphql_handler<Q: ObjectType + 'static, M: ObjectType + 'static, S: SubscriptionType + 'static>(
    schema: Extension<Schema<Q, M, S>>,
    maybe_jwt_claims: Option<JwtClaims<RegisteredClaims>>,
    req: GraphQLRequest,
) -> GraphQLResponse {
    let mut request = req.into_inner();
    if let Some(jwt_claims) = maybe_jwt_claims {
        request = request.data(jwt_claims);
    }
    schema.execute(request).await.into()
}

#[allow(clippy::unused_async)]
pub async fn graphql_ws_handler<Q: ObjectType + 'static, M: ObjectType + 'static, S: SubscriptionType + 'static>(
    schema: Extension<Schema<Q, M, S>>,
    protocol: GraphQLProtocol,
    websocket: WebSocketUpgrade,
    state: State<String>,
) -> impl IntoResponse {
    let secret = state.0;
    let on_connection_init = async move |connection_init_payload: serde_json::Value| {
        #[derive(Deserialize)]
        struct Payload {
            #[serde(rename = "Authorization")]
            bearer_token_string: String,
        }
        if let Ok(payload) = serde_json::from_value::<Payload>(connection_init_payload) {
            let mut data = Data::default();
            let jwt_claims = JwtClaims(
                jsonwebtoken::decode::<RegisteredClaims>(
                    payload.bearer_token_string.trim_start_matches("Bearer "),
                    &DecodingKey::from_secret(secret.as_bytes()),
                    &Validation::default(),
                )?
                .claims,
            );
            tracing::debug!("{:?}", &jwt_claims);
            data.insert(jwt_claims);
            Ok(data)
        } else {
            Err("Failed to init connection - token required".into())
        }
    };
    websocket.protocols(ALL_WEBSOCKET_PROTOCOLS).on_upgrade(move |stream| {
        GraphQLWebSocket::new(stream, schema.0, protocol)
            .on_connection_init(on_connection_init)
            .serve()
    })
}
